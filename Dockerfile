# Build
FROM maven:3.6.0-jdk-11 as build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

# Package
FROM openjdk:11
ENV APP_JAR_NAME carpark-0.0.1-SNAPSHOT

RUN mkdir /app

COPY --from=build /home/app/target/${APP_JAR_NAME}.jar /app
COPY --from=build /home/app/src/main/resources/run.sh /app/

RUN chmod +x /app/run.sh

WORKDIR /app
ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh ./wait-for-it.sh
RUN ["chmod", "+x", "./wait-for-it.sh"]

# Start application
EXPOSE 8080
ENTRYPOINT ["/bin/bash","-c"]
CMD ["/app/run.sh"]
