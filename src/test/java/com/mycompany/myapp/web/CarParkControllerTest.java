package com.mycompany.myapp.web;

import com.mycompany.myapp.CarparkApp;
import com.mycompany.myapp.service.CarParkAvailabilityService;
import com.mycompany.myapp.service.api.dto.CarParkLot;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles({ "unittest" })
@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
    classes = CarparkApp.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-unittest.yml")
public class CarParkControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CarParkAvailabilityService mockServer;
    @Test
    public void carParkNearest_BadRequest() throws Exception {
        mvc.perform(get("/carparks/nearest")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void carParkNearest_isOK() throws Exception {
        var cpl = new CarParkLot();
        cpl.setAddress("BLK 902/908 JURONG WEST STREET 91");
        cpl.setLatitude(1.3408891156031106);
        cpl.setLongitude(103.6854073065924);
        cpl.setTotalLots(700);
        cpl.setAvailableLots(521);
        var exp = Arrays.asList(cpl);
        given(mockServer.carParkNearest(Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any()))
            .willReturn(exp);
        mvc.perform(get("/carparks/nearest?latitude=1&longitude=1&page=2&per_page=3")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].address").value(cpl.getAddress()))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].latitude").value(cpl.getLatitude()))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].longitude").value(cpl.getLongitude()))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].total_lots").value(cpl.getTotalLots()))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].available_lots").value(cpl.getAvailableLots()))
        ;
    }
}
