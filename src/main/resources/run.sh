#!/bin/bash

# The environment variables are already set up by the Dockerfile
./wait-for-it.sh "${DB_HOST}":"${DB_PORT}" -t 60 && java -Duser.timezone=Asia/Saigon -XX:+PrintFlagsFinal $JAVA_OPTIONS -jar ${APP_JAR_NAME}.jar
