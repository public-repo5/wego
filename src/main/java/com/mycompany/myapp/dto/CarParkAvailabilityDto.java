package com.mycompany.myapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CarParkAvailabilityDto {
    @JsonProperty("items")
    private List<Item> items;

    @Data
    public static class CarparkData {

        @JsonProperty("carpark_info")
        private List<CarparkInfo> carparkInfo;
        @JsonProperty("carpark_number")
        private String carparkNumber;
        @JsonProperty("update_datetime")
        private Date updateDatetime;

    }

    @Data
    public static class CarparkInfo {

        @JsonProperty("total_lots")
        private Integer totalLots;
        @JsonProperty("lot_type")
        private String lotType;
        @JsonProperty("lots_available")
        private Integer lotsAvailable;

    }
    @Data
    public static class Item {

        @JsonProperty("timestamp")
        private String timestamp;
        @JsonProperty("carpark_data")
        private List<CarparkData> carparkData;

    }
}
