package com.mycompany.myapp.dto;

public interface CarParkResult {
    String getAddress();

    Double getLatitude();
    Double getLongitude() ;

    Integer getTotalLots();

     Integer getAvailableLots();
}
