package com.mycompany.myapp.domain;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class CarParkLotId implements Serializable {
    private String carParkNo;

    private String lotType;
}
