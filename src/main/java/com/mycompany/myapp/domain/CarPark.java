package com.mycompany.myapp.domain;

import lombok.Data;
import org.locationtech.jts.geom.Point;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class CarPark  {
    @Id
    @Column(length = 16)
    private String carParkNo;
    @Column
    private String address;

    @Column(columnDefinition = "geometry(Point,4326)")
    private Point geometry;

    @Column(length = 64)
    private String carParkType;

    @Column(length = 64)
    private String typeOfParkingSystem;

    @Column(length = 64)
    private String shortTermParking;

    @Column(length = 64)
    private String freeParking;

    @Column(length = 16)
    private String nightParking	;

    @Column
    private Integer carParkDecks;

    @Column
    private Double gantryHeight;

    @Column(length = 16)
    private String carParkBasement;

}
