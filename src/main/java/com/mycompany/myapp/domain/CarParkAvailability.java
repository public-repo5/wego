package com.mycompany.myapp.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(indexes = @Index(columnList = "carParkNo"))
public class CarParkAvailability {
    @EmbeddedId
    private CarParkLotId carParkLotId;

    @Column
    private Date updateDateTime;

    @Column
    private Integer totalLots;

    @Column
    private Integer lotsAvailable;
}
