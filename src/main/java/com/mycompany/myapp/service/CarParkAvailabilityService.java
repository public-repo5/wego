package com.mycompany.myapp.service;

import com.mycompany.myapp.dto.CarParkAvailabilityDto;
import com.mycompany.myapp.service.api.dto.CarParkLot;

import java.math.BigDecimal;
import java.util.List;

public interface CarParkAvailabilityService {
    CarParkAvailabilityDto fetch();
    void update(CarParkAvailabilityDto carParkAvailabilityDto);
    List<CarParkLot> carParkNearest(final Double latitude, final Double longitude, final Integer page, final Integer perPage);
}
