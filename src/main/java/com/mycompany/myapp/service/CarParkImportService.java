package com.mycompany.myapp.service;

import com.mycompany.myapp.dto.CarParkCSV;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;

public interface CarParkImportService {
    List<CarParkCSV> readData(InputStream inputStream) throws IOException;
    void importData(List<CarParkCSV> data);

    boolean isAlreadyImported();
}
