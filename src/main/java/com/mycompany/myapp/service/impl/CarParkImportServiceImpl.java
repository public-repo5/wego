package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.domain.CarPark;
import com.mycompany.myapp.dto.CarParkCSV;
import com.mycompany.myapp.repository.CarParkRepo;
import com.mycompany.myapp.service.CarParkImportService;
import com.mycompany.myapp.util.GeometryUtils;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.qxcg.svy21.SVY21;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CarParkImportServiceImpl implements CarParkImportService {

    @Value("${spring.jpa.properties.hibernate.jdbc.batch_size:25}")
    private int batchSize;

    private final CarParkRepo repo;
    @Override
    public List<CarParkCSV> readData(final InputStream inputStream) throws IOException {
        log.info("Reading data ...");
        try (Reader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            CsvToBean<CarParkCSV> cb = new CsvToBeanBuilder<CarParkCSV>(reader)
                .withType(CarParkCSV.class)
                .build();
            return cb.parse();
        }
    }

    @Override
    @Transactional
    public void importData(final List<CarParkCSV> data) {
        log.info("Importing data size {}", CollectionUtils.size(data));
        var carParkList = data.stream().map(carParkCSV -> {
            var carPark = new CarPark();
            carPark.setCarParkNo(carParkCSV.getCarParkNo());
            carPark.setAddress(carParkCSV.getAddress());
            var latLonCoord = SVY21.computeLatLon(carParkCSV.getYCoord(), carParkCSV.getXCoord());
            carPark.setGeometry(GeometryUtils.of(latLonCoord.getLatitude(), latLonCoord.getLongitude()));
            carPark.setCarParkType(carParkCSV.getCarParkType());
            carPark.setTypeOfParkingSystem(carParkCSV.getTypeOfParkingSystem());
            carPark.setShortTermParking(carParkCSV.getShortTermParking());
            carPark.setFreeParking(carParkCSV.getFreeParking());
            carPark.setNightParking(carParkCSV.getNightParking());
            carPark.setCarParkDecks(carParkCSV.getCarParkDecks());
            carPark.setGantryHeight(carParkCSV.getGantryHeight());
            carPark.setCarParkBasement(carParkCSV.getCarParkBasement());
            return carPark;
        }).collect(Collectors.toList());
        var batches = ListUtils.partition(carParkList, batchSize);
        batches.stream().forEach(carParks -> repo.saveAll(carParks));
        log.info("Finished importing data size {}", CollectionUtils.size(data));
    }

    @Override
    public boolean isAlreadyImported() {
        return repo.count() > 0;
    }
}
