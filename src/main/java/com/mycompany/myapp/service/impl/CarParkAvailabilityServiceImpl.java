package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.client.CarParkAvailabilityClient;
import com.mycompany.myapp.domain.CarParkAvailability;
import com.mycompany.myapp.domain.CarParkLotId;
import com.mycompany.myapp.dto.CarParkAvailabilityDto;
import com.mycompany.myapp.repository.CarParkAvailabilityRepo;
import com.mycompany.myapp.service.CarParkAvailabilityService;
import com.mycompany.myapp.service.api.dto.CarParkLot;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CarParkAvailabilityServiceImpl implements CarParkAvailabilityService {
    @Value("${spring.jpa.properties.hibernate.jdbc.batch_size:25}")
    private int batchSize;
    private final CarParkAvailabilityClient client;
    private final CarParkAvailabilityRepo repo;

    @Override
    public CarParkAvailabilityDto fetch() {
        log.info("Fetching car park availability data ...");
        return client.carParkAvailability();
    }

    @Override
    @Transactional
    public void update(final CarParkAvailabilityDto carParkAvailabilityDto) {
        if (CollectionUtils.isEmpty(carParkAvailabilityDto.getItems())) {
            return;
        }
        var data = carParkAvailabilityDto.getItems().get(0).getCarparkData()
            .stream().flatMap(carparkData ->
                carparkData.getCarparkInfo().stream().map(carparkInfo -> {
                    var carParkAvailability = new CarParkAvailability();
                    var id = new CarParkLotId();
                    id.setCarParkNo(carparkData.getCarparkNumber());
                    id.setLotType(carparkInfo.getLotType());
                    carParkAvailability.setCarParkLotId(id);
                    carParkAvailability.setLotsAvailable(carparkInfo.getLotsAvailable());
                    carParkAvailability.setTotalLots(carparkInfo.getTotalLots());
                    carParkAvailability.setUpdateDateTime(carparkData.getUpdateDatetime());
                    return carParkAvailability;
                })).collect(Collectors.toList());
        var batches = ListUtils.partition(data, batchSize);
        batches.stream().forEach(carParks -> repo.saveAll(carParks));
        log.info("Finish updating  car park availability size {}", data.size());
    }

    @Override
    public List<CarParkLot> carParkNearest(final Double latitude, final Double longitude, final Integer page, final Integer perPage) {
        var data = repo.findNearWithinDistance(longitude.doubleValue(),
            latitude.doubleValue(),
            (page - 1) * perPage,
            perPage);
        return org.apache.commons.collections4.CollectionUtils.emptyIfNull(
            data
        ).stream().map(carParkResult ->
            new CarParkLot().address(carParkResult.getAddress())
                .latitude(carParkResult.getLatitude())
                .longitude(carParkResult.getLongitude())
                .availableLots(carParkResult.getAvailableLots())
                .totalLots(carParkResult.getTotalLots())
        ).collect(Collectors.toList());
    }


}
