package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.CarPark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarParkRepo extends JpaRepository<CarPark, Long> {
}
