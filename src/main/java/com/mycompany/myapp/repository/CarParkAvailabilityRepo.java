package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.CarParkAvailability;
import com.mycompany.myapp.domain.CarParkLotId;
import com.mycompany.myapp.dto.CarParkResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarParkAvailabilityRepo extends JpaRepository<CarParkAvailability, CarParkLotId> {
    @Query(value="select cp.address, ST_X(cp.geometry) as longitude, ST_Y(cp.geometry) as latitude,\n" +
        "ac.total_lots as totalLots,ac.lots_available as availableLots,\n" +
        "ST_Distance(cp.geometry, ST_SetSRID(ST_MakePoint(:lon, :lat), 4326)) as dist\n" +
        "from (select car_park_no, sum(total_lots) as total_lots, sum(lots_available) as lots_available\n" +
        "\t  from car_park_availability \n" +
        "\t  where lots_available > 0\n" +
        "\t  group by car_park_no\n" +
        "\t ) as ac\n" +
        "left join car_park cp\n" +
        "on ac.car_park_no = cp.car_park_no\n" +
        "order by dist asc, lots_available desc, total_lots desc \n" +
        "offset :offset limit :limit", nativeQuery = true)
    List<CarParkResult> findNearWithinDistance(@Param("lon") double lon, @Param("lat") double lat, @Param("offset") int offset, @Param("limit") int limit);
}
