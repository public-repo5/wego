package com.mycompany.myapp.util;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.PrecisionModel;

public class GeometryUtils {
    private static GeometryFactory factory = new GeometryFactory(new PrecisionModel(), 4326);

    private GeometryUtils() {
    }

    public static Point of(double lat, double lon) {
        return factory.createPoint(new Coordinate(lon, lat));
    }
}
