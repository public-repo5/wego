package com.mycompany.myapp.schedule;

import com.mycompany.myapp.service.CarParkAvailabilityService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Profile("!unittest")
@Component
@RequiredArgsConstructor
public class CarParkAvailabilityTask {

    private final CarParkAvailabilityService service;
    @Scheduled(fixedRate = 60000)
    public void updateCarParkAvailability() throws InterruptedException {
        var data = service.fetch();
        service.update(data);
    }

}
