package com.mycompany.myapp.client;

import com.mycompany.myapp.dto.CarParkAvailabilityDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "CarParkAvailabilityClient",url = "https://api.data.gov.sg")
public interface CarParkAvailabilityClient {
    @RequestMapping(method = RequestMethod.GET, value = "/v1/transport/carpark-availability", produces = "application/json")
    CarParkAvailabilityDto carParkAvailability();
}
