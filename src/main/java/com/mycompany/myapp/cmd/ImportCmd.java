package com.mycompany.myapp.cmd;

import com.mycompany.myapp.service.CarParkImportService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Profile("!unittest")
@Component
@RequiredArgsConstructor
public class ImportCmd implements CommandLineRunner {
    private final CarParkImportService importService;

    @Value(value = "classpath:HDBCarparkInformation.csv")
    private Resource csv;

    @Override
    public void run(final String... args) throws Exception {
        if (importService.isAlreadyImported()) {
            return;
        }
        var data = importService.readData(csv.getInputStream());
        importService.importData(data);
    }
}
