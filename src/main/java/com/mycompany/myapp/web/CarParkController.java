package com.mycompany.myapp.web;

import com.mycompany.myapp.service.CarParkAvailabilityService;
import com.mycompany.myapp.service.api.dto.CarParkLot;
import com.mycompany.myapp.web.api.CarparksApiDelegate;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CarParkController implements CarparksApiDelegate {
    private final CarParkAvailabilityService service;
    @Override
    public ResponseEntity<List<CarParkLot>> carParkNearest(final Double latitude, final Double longitude, final Integer page, final Integer perPage) {
        return ResponseEntity.ok(service.carParkNearest(latitude, longitude, page, perPage));
    }
}
