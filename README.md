### System Overview
Technology Stack: JDK11, Maven 3.6, SpringBoot 2.7.3, OpenAPI with swagger 3.0, Postgres 14 with PostGIS extension.
![Car Park Arch](./imgs/carpark-arch.jpg)
- At the fist time, car park service will load car park information from csv into car_park table.
- Update car park availability every minute, store data in car_park_availability table.
- API to find nearest car park from longitude, latitude.

### Getting started
1. Install docker https://docs.docker.com/get-docker/
2. Install docker-compose https://docs.docker.com/compose/install/
3. Install maven http://maven.apache.org/install.html
4. Run run.sh file

### Testing with curl

```
curl -X 'GET' \
  'http://localhost:8080/carparks/nearest?latitude=1.37326&longitude=103.897&page=1&per_page=3' \
  -H 'accept: application/json; charset=utf-8'
```

### Pros and Cons
#### Pros
Implement quickly by using Postgres with PostGIS extension.
### Cons
Low performance when data grows (milions of records).
### Alternative solution
Replace postgres by Redis Cluster to improve searching performance and prevent bottled-neck.
![Car Park Arch](./imgs/carpark-arch-alt.jpg)
